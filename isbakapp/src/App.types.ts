export interface ITrafficLight {
    group1: 'red' | 'yellow' | 'green';
    group2: 'red' | 'yellow' | 'green';
    pedestrian: 'red' | 'yellow' | 'green';
}

export interface ITrafficLightConfig {
    id: keyof ITrafficLight;
    colors: Array<'red' | 'yellow' | 'green'>;
}
