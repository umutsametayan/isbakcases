import { useState, useEffect, useCallback } from 'react';

import { TrafficLight, Incrementor } from './components';
import { config, initialState, steps } from './App.config';
import { ITrafficLight } from './App.types';
import PostsList from './posts/postlist'

import './App.css';

function App() {
    const [currentStep, setCurrentStep] = useState(0);
    const [lightState, setLightState] = useState<ITrafficLight>(initialState);
    const [greenDuration, setGreenDuration] = useState(5);
    const [countdown, setCountdown] = useState<number>(5);
    const [caseStatus, setCaseStatus] = useState<boolean>(false);

    useEffect(() => {
        if (countdown <= 0) {
            const nextStep = (currentStep + 1) % steps.length;

            setCurrentStep(nextStep);
            setLightState(steps[nextStep]);

            const nextDuration = nextStep === 1 || nextStep === 3 ? greenDuration : 5;

            setCountdown(nextDuration);
        } else {
            const timerId = setTimeout(() => {
                setCountdown(countdown - 1);
            }, 1000);

            return () => clearTimeout(timerId);
        }
    }, [countdown, currentStep, greenDuration]);

    const handlePedestrianPress = useCallback(() => {
        setLightState({ group1: 'yellow', group2: 'yellow', pedestrian: 'green' });
        setCountdown(5);

        setTimeout(() => {
            setLightState({ group1: 'red', group2: 'red', pedestrian: 'green' });
            setCountdown(10);

            setTimeout(() => {
                setLightState({ group1: 'red', group2: 'red', pedestrian: 'red' });
                setCurrentStep(0);
                setCountdown(greenDuration);
            }, 10000);
        }, 5000);
    }, []);

    return (
        <>
            {caseStatus ? <PostsList caseStatus = {caseStatus} setCaseStatus={setCaseStatus} /> 
            :
                <div className='centered'>
                    <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: '100%' }}>
                        <div className='flex-1'></div>
                        <div className='heading'>
                            <h1>Trafik Sinyal Kontrolü</h1>
                        </div>
                        <div className='change-case-container'>
                            <button onClick={() => setCaseStatus(!caseStatus)} className='change-case stage-btn mr'> {caseStatus ? "Case 1 'e dön" : "Case 2 'ye git"} </button>
                        </div>
                    </div>
                    <div>
                        <div className='greenlight-set-area'>
                            <div>
                                <h3>Yeşil Işık Süresini Ayarla</h3>
                            </div>
                            <div>
                                <Incrementor setGreenDuration={setGreenDuration} initial={greenDuration} />
                            </div>
                        </div>
                        <>
                            <br />
                            <button className='Pedestrain-btn' onClick={handlePedestrianPress}>Yaya Buttonu</button>
                        </>
                        <>
                            <p className='show-steps'>Mevcut Adım : {currentStep + 1}</p>
                            <p className='show-steps'>Sonraki Adıma Kalan Süre : {countdown} saniye</p>
                        </>
                    </div>
                    <div className="stoplights">
                        {config.map((trafficLight) => (
                            <TrafficLight key={trafficLight.id} trafficLight={trafficLight} state={lightState} />
                        ))}
                    </div>
                </div>}
        </>
    );
}

export default App;
