import { ITrafficLight, ITrafficLightConfig } from '../../App.types';

export interface ITrafficLightProps {
    trafficLight: ITrafficLightConfig;
    state: ITrafficLight;
}
