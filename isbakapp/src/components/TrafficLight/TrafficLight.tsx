import React from 'react';

import { ITrafficLightProps } from './TrafficLight.types';

function TrafficLight(props: ITrafficLightProps) {
    const { trafficLight, state } = props;

    return (
        <div className="traffic-light">
            <div className="traffic-light__bulbs">
                {trafficLight.colors.map((color) => (
                    <span key={color} className={`${state[trafficLight.id] === color ? `${color}-on` : `${color}-off`}`} />
                ))}
            </div>

            <span className="traffic-light__pole" />
        </div>
    );
}

export default TrafficLight;
