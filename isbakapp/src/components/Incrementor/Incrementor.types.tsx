import { Dispatch,SetStateAction } from "react";

export default interface IIncrementorProps {
    setGreenDuration:Dispatch<SetStateAction<number>>
    initial?: number;
    step?: number;
    min?: number;
    max?: number;
}