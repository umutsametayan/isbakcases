import React, { useState } from 'react';
import axios from 'axios';
import { useGetPostsQuery } from './postsapi';
import PostDetail from './postdetail'

interface Post {
  userId: number;
  id: number;
  title: string;
  body: string;
}

interface PostsListProps {
  setCaseStatus: (status: boolean) => void;
  caseStatus: boolean;
}

const PostsList: React.FC<PostsListProps> = ({ setCaseStatus, caseStatus }) => {
  const [isShow, setIsShow] = useState<boolean>(false);
  const [postDetail, setPostDetail] = useState<Post | null>(null);

  const getPostDetail = async (postId: number) => {
    try {
      const response = await axios.get<Post>(`https://jsonplaceholder.typicode.com/posts/${postId}`);
      setIsShow(true);
      setPostDetail(response.data);
    } catch (error) {
      console.error("Error occurred while fetching post details:", error);
      throw error;
    }
  };

  const { data: posts, error, isLoading } = useGetPostsQuery();

  if (isLoading) return <div>Loading...</div>;
  if (error) return <div>Error occurred</div>;

  return (
    <>
      {isShow && postDetail ? (
        <PostDetail postId={postDetail?.id || 0} />
      ) : (
        <>
          <button onClick={() => setCaseStatus(!caseStatus)} className='change-case stage-btn mr'>
            {caseStatus ? "Case 1' e dön" : "case2"}
          </button>
          <ul className="post-list">
            {posts?.map((post) => (
              <li
                className="post-item"
                key={post.id}
                onClick={() => getPostDetail(post.id)}
              >
                {post.title}
              </li>
            ))}
          </ul>
        </>
      )}
    </>
  );
};

export default PostsList;
