import React from 'react';
import { useGetPostByIdQuery } from './postsapi';

interface PostDetailsProps {
  postId: number;
}

const PostDetails: React.FC<PostDetailsProps> = ({ postId }) => {
  const { data: post, error, isLoading } = useGetPostByIdQuery(postId);

  if (isLoading) return <div>Loading...</div>;
  if (error) return <div>Error occurred</div>;
  if (!post) return <div>No post found</div>;

  return (
    <div className='centered'>
      <button className='stage-btn' onClick = {() => window.location.reload()}>Başa Dön</button>
      <h1>{post.title}</h1>
      <p>{post.body}</p>
    </div>
  );
};

export default PostDetails;
